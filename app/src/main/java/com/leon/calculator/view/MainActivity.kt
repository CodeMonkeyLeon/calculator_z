package com.leon.calculator.view

import android.text.Editable
import android.text.InputType
import android.text.TextUtils
import android.text.TextWatcher
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.leon.calculator.R
import com.leon.calculator.adapter.ResultAdapter
import com.leon.calculator.base.BaseAbstractActivity
import com.leon.calculator.entity.ResultData
import com.leon.calculator.utils.Constants
import com.leon.calculator.utils.StorageUtils

class MainActivity : BaseAbstractActivity() {

    override fun getLayoutId() = R.layout.act_main


    private var mOperatorType: Int = Constants.TYPE_ADD

    private lateinit var mTvOperator: TextView
    private lateinit var mEtNum1: EditText
    private lateinit var mEtNum2: EditText
    private lateinit var mTvResult: TextView

    private lateinit var mRecyclerView: RecyclerView
    private lateinit var mAdapter: ResultAdapter

    private lateinit var mBtnRecord: Button

    private val mRecordList = ArrayList<ResultData>()

    private fun calculator(num1: Float, num2: Float) {
        when (mOperatorType) {
            Constants.TYPE_ADD -> {
                mTvResult.text = "${num1 + num2}"
            }

            Constants.TYPE_SUBTRACT -> {
                mTvResult.text = "${num1 - num2}"
            }

            Constants.TYPE_MULTIPLY -> {
                mTvResult.text = "${num1 * num2}"
            }

            Constants.TYPE_DIVIDE -> {
                mTvResult.text = "${num1 / num2}"
            }

            else -> {
                mTvResult.text = ""
            }
        }
    }

    private val mTextWatcher1 by lazy {
        object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

            override fun afterTextChanged(s: Editable?) {
                try {
                    if (!TextUtils.isEmpty(s.toString()) && !TextUtils.isEmpty(mEtNum2.text)) {
                        val num1 = s.toString().toFloat()
                        val num2 = mEtNum2.text.toString().toFloat()
                        calculator(num1, num2)
                    } else {
                        mTvResult.text = ""
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                    mTvResult.text = ""
                }
            }
        }
    }

    private val mTextWatcher2 by lazy {
        object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

            override fun afterTextChanged(s: Editable?) {
                try {
                    if (!TextUtils.isEmpty(s.toString()) && !TextUtils.isEmpty(mEtNum1.text)) {
                        val num1 = mEtNum1.text.toString().toFloat()
                        val num2 = s.toString().toFloat()
                        calculator(num1, num2)
                    } else {
                        mTvResult.text = ""
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                    mTvResult.text = ""
                }
            }
        }
    }


    override fun initView() {

        mTvOperator = findViewById(R.id.id_tv_operator)
        mEtNum1 = findViewById(R.id.id_et_num_1)
        mEtNum2 = findViewById(R.id.id_et_num_2)
        mTvResult = findViewById(R.id.id_tv_result)
        mBtnRecord = findViewById(R.id.id_btn_record)

        mEtNum1.inputType = InputType.TYPE_CLASS_NUMBER or InputType.TYPE_NUMBER_FLAG_DECIMAL
        mEtNum2.inputType = InputType.TYPE_CLASS_NUMBER or InputType.TYPE_NUMBER_FLAG_DECIMAL

        mEtNum1.addTextChangedListener(mTextWatcher1)
        mEtNum2.addTextChangedListener(mTextWatcher2)

        R.id.id_btn_add.setOperatorType(Constants.TYPE_ADD)
        R.id.id_btn_subtract.setOperatorType(Constants.TYPE_SUBTRACT)
        R.id.id_btn_multiply.setOperatorType(Constants.TYPE_MULTIPLY)
        R.id.id_btn_divide.setOperatorType(Constants.TYPE_DIVIDE)

        R.id.id_img_del_1.delete(Constants.NUM_1)
        R.id.id_img_del_2.delete(Constants.NUM_2)

        mBtnRecord.setOnClickListener {
            val res = mTvResult.text.toString().trim()
            if (!TextUtils.isEmpty(res)) {
                val old = StorageUtils.getDataFromInternalStorage(this@MainActivity)
                var new = "$res${StorageUtils.SPLIT}"
                if (!TextUtils.isEmpty(old)) {
                    new = "$res${StorageUtils.SPLIT}$old"
                }
                StorageUtils.saveDataToInternalStorage(this@MainActivity, new)
                mRecordList.add(0, ResultData(mTvResult.text.toString().trim()))
                mAdapter.notifyDataSetChanged()
            }
        }

        initRecyclerView()
    }


    val initRecyclerView = {
        mRecordList.clear()
        StorageUtils.getDataFromInternalStorage(this@MainActivity)?.run {
            val array = split(StorageUtils.SPLIT)
            for (str in array) {
                if (!TextUtils.isEmpty(str)) {
                    mRecordList.add(ResultData(str))
                }
            }
        }

        mRecyclerView = findViewById(R.id.id_recycler_view)

        val linearLayoutManager = LinearLayoutManager(this)
        linearLayoutManager.orientation = RecyclerView.VERTICAL

        mAdapter = ResultAdapter(mRecordList)

        mRecyclerView.layoutManager = linearLayoutManager
        mRecyclerView.adapter = mAdapter
    }


    private fun Int.delete(num: Int) {
        findViewById<ImageView>(this).setOnClickListener {
            when (num) {
                Constants.NUM_1 -> {
                    mEtNum1.setText("")
                }

                Constants.NUM_2 -> {
                    mEtNum2.setText("")
                }

                else -> {

                }
            }
        }
    }


    private fun calculate() {
        val et1 = mEtNum1.text.toString()
        val et2 = mEtNum2.text.toString()

        if (!TextUtils.isEmpty(et1) && !TextUtils.isEmpty(et2)) {
            try {
                val num1 = et1.toFloat()
                val num2 = et2.toFloat()
                calculator(num1, num2)
            } catch (e: Exception) {
                e.printStackTrace()
                mTvResult.text = ""
            }


        } else {
            mTvResult.text = ""
        }
    }


    private fun Int.setOperatorType(type: Int) {
        findViewById<Button>(this).setOnClickListener {
            mOperatorType = type
            calculate()
            when (mOperatorType) {
                Constants.TYPE_ADD -> {
                    mTvOperator.text = "+"
                }

                Constants.TYPE_SUBTRACT -> {
                    mTvOperator.text = "-"
                }

                Constants.TYPE_MULTIPLY -> {
                    mTvOperator.text = "×"
                }

                Constants.TYPE_DIVIDE -> {
                    mTvOperator.text = "÷"
                }

                else -> {

                }
            }
        }
    }

}