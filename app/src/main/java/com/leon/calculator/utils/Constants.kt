package com.leon.calculator.utils

object Constants {

    const val ITEM_TYPE = 0;

    const val NUM_1 = 1001

    const val NUM_2 = 1002

    const val TYPE_ADD = 1

    const val TYPE_SUBTRACT = 2

    const val TYPE_MULTIPLY = 3

    const val TYPE_DIVIDE = 4


}