package com.leon.calculator.utils

import android.content.Context
import android.util.Log
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream


object StorageUtils {

    const val DIR_NAME = "recordDir"
    const val FILE_NAME = "record.txt"
    const val SPLIT = "###"

    /**
     * 保存数据至内部存储
     *
     * @param context
     * @param data
     * @param dirName
     * @param fileName
     */
    fun saveDataToInternalStorage(
        context: Context,
        data: String
    ) {
        try {
            val dir = context.getDir(DIR_NAME, Context.MODE_PRIVATE)
            val file = File(dir, FILE_NAME)
            val fos = FileOutputStream(file)
            fos.write(data.toByteArray())
            fos.flush()
            fos.close()
        } catch (e: Exception) {
            e.printStackTrace()
            Log.e("TAG_ERROR", e.message!!)
        }
    }


    /**
     * 获取数据从内部存储
     *
     * @param context
     * @param dirName
     * @param fileName
     * @return
     */
    fun getDataFromInternalStorage(context: Context): String? {
        try {
            val dir = context.getDir(DIR_NAME, Context.MODE_PRIVATE)
            val file = File(dir, FILE_NAME)
            val fis = FileInputStream(file)
            val readData = ByteArray(fis.available()) //根据文件大小创建数组
            while (fis.read(readData) != -1) {
                //do nothing
            }
            fis.close()
            return String(readData)
        } catch (e: Exception) {
            e.printStackTrace()
            Log.e("TAG_ERROR", e.message!!)
        }
        return null
    }
}