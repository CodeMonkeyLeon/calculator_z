package com.leon.calculator.viewholder

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.leon.calculator.R

class ResultViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    private lateinit var mTvResult: TextView


    init {
        mTvResult = itemView.findViewById(R.id.id_tv_item_result)
    }


    fun setText(result: String) {
        mTvResult.text = result
    }
}