package com.leon.calculator.base

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import androidx.appcompat.app.AppCompatActivity

abstract class BaseAbstractActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        configView()
        initView()
    }


    private fun configView() {
        val inflater = LayoutInflater.from(this)
        val baseView = inflater.inflate(getLayoutId(), null)
        setContentView(baseView)
        hideToolBar()
    }


    /**
     * 隐藏 ToolBar
     */
    private fun hideToolBar() {
        supportActionBar?.run {
            hide()
        }
    }


    protected abstract fun getLayoutId(): Int

    protected abstract fun initView()



    protected fun log(msg:String){
        Log.i("TAG_ZLZ", msg)
    }
}