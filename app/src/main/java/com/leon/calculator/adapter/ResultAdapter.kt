package com.leon.calculator.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.leon.calculator.R
import com.leon.calculator.entity.ResultData
import com.leon.calculator.viewholder.ResultViewHolder

class ResultAdapter(
    private val mList: ArrayList<ResultData>
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ResultViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_record, parent, false),
        )
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val resultViewHolder = holder as ResultViewHolder
        resultViewHolder.setText(mList[position].result)
    }

    override fun getItemCount() = mList.size
}