package com.leon.calculator.entity

import java.io.Serializable

data class ResultData(
    val result: String = ""
) : Serializable
